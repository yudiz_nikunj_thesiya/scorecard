import React, { useEffect } from "react";
import { Route, Routes, useLocation } from "react-router-dom";
import { ReactQueryDevtools } from "react-query/devtools";
import Home from "./pages/Home";
import ReactGA from "react-ga";

function App() {
	const TRACKING_ID = "UA-226047133-2";
	ReactGA.initialize(TRACKING_ID);

	const location = useLocation();

	useEffect(() => {
		ReactGA.pageview(location.pathname);
	}, []);

	return (
		<div className="App">
			<Routes>
				<Route path="/" element={<Home />} />
			</Routes>
			<ReactQueryDevtools initialIsOpen={false} position="bottom-right" />
		</div>
	);
}

export default App;
