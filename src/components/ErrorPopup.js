import React from "react";
import ReactDom from "react-dom";
import PropTypes from "prop-types";
import "../styles/errpopup.scss";

const ErrorPopup = ({ setErrPopup, message }) => {
	return ReactDom.createPortal(
		<div className="modal-container">
			<div className="modal">
				<div className="modal-header">
					<span className="heading">{message}</span>
					<span className="icon" onClick={() => setErrPopup(false)}>
						x
					</span>
				</div>
			</div>
		</div>,
		document.getElementById("portal")
	);
};

export default ErrorPopup;

ErrorPopup.propTypes = {
	setErrPopup: PropTypes.func,
	message: PropTypes.string,
};
