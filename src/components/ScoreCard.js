import React from "react";
import "../styles/scorecard.scss";
import moment from "moment";
import PropTypes from "prop-types";
import winnerCup from "../assets/winner-cup.ab728cae47f4b716f5e2.svg";
import team1 from "../assets/sr_team_3420.svg";
import team2 from "../assets/sr_team_3433.svg";

const ScoreCard = ({ data }) => {
	const { venue, scheduled, home, away } = data;
	const matchDate = moment(scheduled).format("Do MMMM YYYY");

	return (
		<div className="scorecard">
			{/* ----- Card headline ------- */}
			<div className="scorecard-headline">
				{`${venue?.name}, ${venue?.city} | ${matchDate}`}
			</div>
			{/* ------ card body ----- */}
			<div className="scorecard-body">
				{/* ----- card body left side content ------ */}
				<div className="scorecard-body-left">
					<img src={team1} alt="logo" className="team-logo" />
					<div className="team-name">
						<span>{`${home?.market} ${home?.name}`}</span>
						<img src={winnerCup} alt="winner cup" />
					</div>
					<span className="team-score">{`${home?.points}`}</span>
				</div>

				{/* ----- card body middle side content ------ */}
				<div className="scorecard-body-middle">
					<span className="label">FINAL</span>
					<table className="table">
						<thead>
							<tr>
								<th></th>
								{home?.scoring.map((score) => (
									<td key={score?.number}>{score?.number}</td>
								))}
								<th>T</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td className="heading">{home?.alias}</td>
								{home?.scoring.map((score) => (
									<td key={score?.number}>{score?.points}</td>
								))}
								<td>{home?.points}</td>
							</tr>
							<tr>
								<td className="heading">{away?.alias}</td>
								{away?.scoring.map((score) => (
									<td key={score?.number}>{score?.points}</td>
								))}
								<td>{away?.points}</td>
							</tr>
						</tbody>
					</table>
				</div>

				{/* ----- card body right side content ------ */}
				<div className="scorecard-body-right">
					<img src={team2} alt="logo" className="team-logo" />
					<div className="team-name">
						<span>{`${away?.market} ${away?.name}`}</span>
					</div>
					<span className="team-score">{`${away?.points}`}</span>
				</div>
			</div>

			<div className="scorecard-table">
				<span className="label">FINAL</span>
				<table className="table">
					<thead>
						<tr>
							<th></th>
							{home?.scoring.map((score) => (
								<td key={score?.number}>{score?.number}</td>
							))}
							<th>T</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td className="heading">{home?.alias}</td>
							{home?.scoring.map((score) => (
								<td key={score?.number}>{score?.points}</td>
							))}
							<td>{home?.points}</td>
						</tr>
						<tr>
							<td className="heading">{away?.alias}</td>
							{away?.scoring.map((score) => (
								<td key={score?.number}>{score?.points}</td>
							))}
							<td>{away?.points}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	);
};

export default ScoreCard;

ScoreCard.propTypes = {
	data: PropTypes.object,
};
