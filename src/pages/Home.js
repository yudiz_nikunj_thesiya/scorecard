import React, { useEffect, useState } from "react";
import { useQuery } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { setData } from "../app/action-creators";
import ScoreCard from "../components/ScoreCard";
import { Helmet } from "react-helmet";
import "../styles/home.scss";
import ErrorPopup from "../components/ErrorPopup";

const fetchData = async () => {
	const res = await fetch(
		"https://backend.sports.info/api/v1/nba/game/f34b1dfd-97fd-4942-9c14-05a05eeb5921/summary"
	);

	return res.json();
};

const Home = () => {
	const [errPopup, setErrPopup] = useState(false);
	const { data, isSuccess, isLoading, isError, error } = useQuery(
		"data",
		fetchData
	);
	const dispatch = useDispatch();
	const apiData = useSelector((state) => state?.data);

	useEffect(() => {
		isSuccess && dispatch(setData(data?.data));
		isError && setErrPopup(true);
	}, [data]);

	return (
		<div className="card-container">
			<Helmet>
				<meta charSet="utf-8" />
				<title>ScoreCard</title>
				<meta
					name="description"
					content="scorecard is a react app where you can see the score of matches."
				/>
			</Helmet>

			{isLoading ? (
				<div>Loading data.....</div>
			) : (
				<ScoreCard data={apiData?.data} />
			)}

			{errPopup && (
				<ErrorPopup setErrPopup={setErrPopup} message={error?.message} />
			)}
		</div>
	);
};

export default Home;
